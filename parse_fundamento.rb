#!/usr/bin/ruby

require 'csv'
require 'nokogiri'
# require 'open-uri'

def add_entry(csv, vortarero)
    eo = vortarero.at_xpath('strong/a').text
    csv << vortarero.xpath('span').collect {|e| e.text.gsub(/\n/, '').strip.gsub(/\s+/, ' ')}.unshift(eo)
end

# doc = Nokogiri::HTML.parse(open('https://www.akademio-de-esperanto.org/fundamento/universala_vortaro.html')
doc = Nokogiri::HTML.parse(open('vortaro.xhtml'))
vortaro = doc.xpath('/html/body/div/div[1]/div[position()>1]/ul/li')

CSV.open('word_list.csv', 'wb', col_sep: '|') do |csv|
    csv << ['EO','FR','EN','DE','RU','PL']

    for vortarero in vortaro
        add_entry(csv, vortarero)

        vortarero.xpath('ul/li').each do |sub|
            add_entry(csv, sub)
        end
    end
end
